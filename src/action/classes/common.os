Функция ПриОпределенииКлассов() Экспорт

	ДополнительныеКлассы = Новый Массив();
	ДополнительныеКлассы.Добавить("Действия");
	ДополнительныеКлассы.Добавить("Клавиатуры");
	ДополнительныеКлассы.Добавить("КомандыСистемы");
	
	Возврат ДополнительныеКлассы;
	
КонецФункции

Процедура ПриИнициализации() Экспорт

	УстановитьПеременныеСреды();

	УстановитьКоманды();

КонецПроцедуры

Процедура УстановитьПеременныеСреды()

КонецПроцедуры

Процедура УстановитьКоманды()

	МассивКоманды = Команды();

	Если ЗначениеЗаполнено(МассивКоманды) Тогда
		
		Ответ = НастройкиПриложения.Бот.УстановитьКоманды(МассивКоманды);
		
		Если НЕ Ответ["ok"] Тогда
			НастройкиПриложения.Бот.Ошибка("Команды не установлены");
		КонецЕсли;

	КонецЕсли;

КонецПроцедуры

Функция Команды() Экспорт

	МассивКоманды = Новый Массив();

	РасположениеМодулейЛогики = НастройкиПриложения.Параметры.РасположениеМодулейЛогики;

	// содержимое файла command.txt можно копировать в команду в BotFather
	ИмяФайлаКоманд = ОбщегоНазначения.НайтиФайл(РасположениеМодулейЛогики, "command.txt");

	Если ЗначениеЗаполнено(ИмяФайлаКоманд) Тогда

		Текст = ОбщегоНазначения.ПрочитатьТекстовыйФайл(ИмяФайлаКоманд);

		Для инд = 1 По СтрЧислоСтрок(Текст) Цикл
			Строка = СтрПолучитьСтроку(Текст, инд);
			Если НЕ ПустаяСтрока(Строка) Тогда

				Поз = СтрНайти(Строка, " - ");
				сткКоманда = ТелеграмАПИ.КомандаБота(Лев(Строка, Поз-1), Сред(Строка, Поз+3));
				
				МассивКоманды.Добавить(сткКоманда);

			КонецЕсли;
		КонецЦикла;

	КонецЕсли;

	Возврат МассивКоманды;

КонецФункции
